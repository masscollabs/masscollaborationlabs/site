<img src="/image/logo.png" alt="drawing" width="90"/> 

## Milis Linux - Milis Operating System

Milis Linux is a Linux kernel based operating system project that was initiated voluntarily in 2016, considering the inadequate and interrupted domestic operating system studies in our country (Turkey), and supported by the Akdeniz University as of 2019.

Milis was developed by making use of LFS, namely "Linux From Scratch" construction technique in order to achieve the originality of Linux. Accordingly, it has own unique package building and management system (Milis Package System) and own applications. 

### Milis Linux 2.1 (GenghisKhan) Key Features:

- Supports x86-64 bit also RISC-V under construction
- MPS Package Manager coded with Lua
- Wayland as default graphic server
- Ayguci tool (modular setting system)
- Militer Init system (Busybox init + Lua service scripts)
- CLI applications with various languages (Rust, Go, Zig)
- Readable INI file format build templates
- Clean and stable system (You know what works)
- Volunteer and productive team support
