<!-- title: Doc -->
### Documents

- [Milis Package System](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/mps.md)
- [Service Management](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/servis.md)
- [Desktop Environment and Default Applications](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/masaustu.md)
- [Ayguci Tool](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/ayguci.md)
- [System Test](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/sistem_test.md)
- [Live Image Production](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/imaj-uretimi.md)
- [Build Environment Production](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/derleme-ortami-uretme.md)
- [Milis Linux from Scratch](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/mlfs.md)
- [Eduroam Connection](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/eduroam.md)

### Academic Studies

- [Building of a Linux Based Lightweight Open Source Big Data Distribution](https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=aEzj_IdWAsjiSAfK3qwrBtYatSKohvG_E7ljeRqDQ5VygI1_MRFprtVbLB0wlGpY)
- [Building An Open Source Linux Computing System On RISC-V](https://ieeexplore.ieee.org/document/8965559)
- [Blockchain Based Distributed Package Management Architecture](https://ieeexplore.ieee.org/document/9219374)
- [Building an Open Source Big Data Platform Based on Milis Linux](https://link.springer.com/chapter/10.1007/978-3-030-79357-9_5)
