<!-- title: İletişim -->
### İletişim
- [E-posta](mailto:milisarge@gmail.com)
- [Forum](http://mls.akdeniz.edu.tr/forum/forum)
- [GIT](https://mls.akdeniz.edu.tr/git/milislinux/milis21)
- [Konuşma Ortamı (Mattermost)](https://mls.akdeniz.edu.tr/mm)
- [Sosyal Ortam (Mastodon)](https://mastodon.social/@milislinux)
- [Konuşma Ortamı (IRC)](https://kiwiirc.com/nextclient/irc.libera.chat/#milislinux)
- [Konuşma Ortamı (Telegram)](https://t.me/joinchat/d-Uuyc0ubDExOTU0)

**Not**: Az da olsa devamlı katkı vermek isteyen arkadaşlar GIT ve Mattermost kaydı için milisarge at gmail.com ile irtibata geçebilir.
