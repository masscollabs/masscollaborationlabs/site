<!-- title: İndir -->
## Milis Linux 2.3 Sürümleri

- [Milis 2.3 Masaüstü Beta (Kasım 2023)](https://mls.akdeniz.edu.tr/iso/2.3/beta/milis-2.3-desktop-2023.11.13.iso) -
[shasum](https://mls.akdeniz.edu.tr/iso/2.3/beta/milis-2.3-desktop-2023.11.13.iso.sha256sum.txt)

- [Milis 2.3 Minimal Kararlı (Kasım 2023)](https://mls.akdeniz.edu.tr/iso/2.3/milis-2.3-minimal-2023.11.07.iso) -
[shasum](https://mls.akdeniz.edu.tr/iso/2.3/milis-2.3-minimal-2023.11.07.iso.sha256sum.txt)

## Milis Linux 2.1 Sürümleri

- [Milis 2.1 Masaüstü Kararlı (Şubat 2023)](https://mls.akdeniz.edu.tr/iso/2.1/milis-2.1-desktop-2023.02.02.iso) -
[shasum](https://mls.akdeniz.edu.tr/iso/2.1/milis-2.1-desktop-2023.02.02.iso.sha256sum.txt)

- [Milis 2.1 Minimal Kararlı (Mart 2022)](https://mls.akdeniz.edu.tr/iso/2.1/milis-2.1-minimal-2022.03.27.iso) -
[shasum](https://mls.akdeniz.edu.tr/iso/2.1/milis-2.1-minimal-2022.03.27.iso.sha256sum.txt)


### Giriş Bilgileri

<pre>giriş : mls / şifre : mls </pre>

Milis Linux'u ilk açtığınızda ağ bağlantınızı yaptıktan sonra terminal uygulamasını açıp:

<pre>sudo mps gun</pre>

komutunu verdiğinizde son güncel değişiklikler çekilecektir.

Kurulum için bu [bağlantıdan](https://mls.akdeniz.edu.tr/doc/kurulum) yararlanabilirsiniz.

**Not:** Bu sürüm öntanımlı olarak Wayland grafik sunucusu kullanmaktadır.

### Sistem Gereksinimleri
64-bit - 1.0GHz+ CPU, 1024MB+ RAM, 8GB+ disk alanı, en az 800×600 çözünürlük destekli GPU.

### Sanal Makine Ayarları
Canlı imaj, sanal makine ile de başlatılıp kullanılabilir.
Fakat sanal makine başlatılırken bazı parametrelerin verilmesi gerekmektedir.

    - Qemu: "-vga qxl" parametresi eklenmeli.
    - Virtualbox: Graphisc Controller olarak VMSVGA ve 3D accelaration enable seçili olmalıdır.

--------------------------------------
