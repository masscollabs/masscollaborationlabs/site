<!-- title: Belge -->
### Belgeler

- [Katkı ve Destek Notları](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/destek.md)
- [Kurulum](/doc/kurulum)
- [Milis Paket Sistemi](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/mps.md)
- [Servis Yönetimi](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/servis.md)
- [Masaüstü Ortamı ve Varsayılan Uygulamalar](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/masaustu.md)
- [Ayguci Modüler Ayar Sistemi](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/ayguci.md)
- [Saat/Tarih Ayarı](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/saat_tarih.md)
- [Masaüstü Kısayolları](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/kisayollar.md)
- [Sistem Test Maddeleri](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/sistem_test.md)
- [Paketleme](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/paketleme.md)
- [Canlı/Kurulum İmaj Üretimi](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/imaj-uretimi.md)
- [Derleme Ortamı Üretimi](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/derleme-ortami-uretme.md)
- [Milis 2.1 Sıfırdan Yapım Rehberi](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/mlfs.md)
- [Masaüstü Paylaşımı](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/masa-paylasim.md)
- [Uzak Masaüstü Bağlantısı](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/uzak-masa.md)
- [Virt-Manager Kurulumu](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/virt-manager.md)
- [Nginx+PHP+MariaDB+PostgreSQL Kurulumu](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/nginx-php.md)
- [Eduroam Bağlantısı](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/eduroam.md)


### Akademik Çalışmalar

- [Linux Tabanlı Hafif ve Açık Kaynak Kodlu Büyük Veri Dağıtımı Gerçeklemesi](https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=aEzj_IdWAsjiSAfK3qwrBtYatSKohvG_E7ljeRqDQ5VygI1_MRFprtVbLB0wlGpY)
- [Building An Open Source Linux Computing System On RISC-V](https://ieeexplore.ieee.org/document/8965559)
- [Blockchain Based Distributed Package Management Architecture](https://ieeexplore.ieee.org/document/9219374)
- [Building an Open Source Big Data Platform Based on Milis Linux](https://link.springer.com/chapter/10.1007/978-3-030-79357-9_5)

