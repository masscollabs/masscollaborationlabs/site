<!-- title: Milis Linux 2.1 Kararlı Sürüm Duyurusu -->

<!--2022/02/19-->
Yaklaşık 6 aylık yoğun ön hazırlık sistem çalışmalarımız ardından nihayet Milis Linux 2.1 sisteminin kararlı sürümünü hazırlamış bulunmaktayız. 
Bu zaman zarfında özellikle masaüstü ortamının tasarım ve işlevselliğine dönük birçok düzeltme ve geliştirme yapıldığı gibi
MPS paket yöneticisinin yeni sürümüne güncellenmesiyle paketlerin gerek tespiti ve kurulum süreçlerinde performans artışı sağlanmıştır.
Sistemin tek yerden yönetilebilmesi için Ayguci adlı ayar merkezi uygulamamızın geliştirilmesine hız verilerek kararlı sürümde yer alması sağlanmıştır.
Masaüstünde kullanılan resim görüntüleme ve ekran alıntılama gibi işler için özgün uygulamalar geliştirilerek varsayılan uygulamalar olarak kullanılması sağlanmıştır.
Ek olarak çoğu uygulamanın Türkçe dil çevirileri tamamlanmaya çalışılmıştır. 
Bundan sonra Milis Linux, bu sistem üzerinden kararlı-yuvarlanan şekilde geliştirilmeye devam edilecektir. 
Son olarak Milis Linux 2.1 sürümünü ülkemizin faydasına sunarken bu sürümün hazırlanmasında emeği geçen destekçi arkadaşlarımıza da ayrıca teşekkür ederiz.

-----------

19-02-2022 tarihli hazırlanan kararlı sistem için ayrıntılı bilgiler aşağıda belirtilmiştir. 

### Uygulanan İlkeler

- Kararlı ve aynı zamanda güncel sistemin sağlanması.
- Günlük kullanım için gerekli uygulamalara yer verilmesi.
- Az kaynak tüketen bir masaüstü ortamının oluşturulması.
- Masaüstünde sade ve işlevsel bir tasarımın uygulanması.
- Sorunsuz güncellenebilir bir yapının elde edilmesi.
- Tek ayar merkeziyle sistemin yönetilebilmesi.
- Geliştirme sürecinde devamlılığın esas alınması.

### Varsayılan Uygulamalar

- Pencere yöneticisi: [wayfire](https://github.com/WayfireWM/wayfire)
- Panel : [waybar](https://github.com/Alexays/Waybar)
- Menü: [nwg-launchers](https://github.com/nwg-piotr/nwg-launchers)
- Ayar Merkezi: [ayguci](https://mls.akdeniz.edu.tr/git/milislinux/ayguci), [ayguciUI](https://mls.akdeniz.edu.tr/git/milislinux/ayguciui)
- Paket yönetim arayüzü: [MPS-UI](https://mls.akdeniz.edu.tr/git/milislinux/mps-ui)
- Açılış(init) sistemi: [militer](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/ayarlar/init)
- Servis yönetim arayüzü: [mservice](https://mls.akdeniz.edu.tr/git/milislinux/mservice)
- Giriş yöneticisi: [ly](https://github.com/nullgemm/ly)
- Oturum yöneticisi: [seatd](https://git.sr.ht/~kennylevinsen/seatd)
- Bildirim sunucusu: [fnott](https://codeberg.org/dnkl/fnott)
- Ses sunucusu: [pipewire](https://pipewire.org)
- Ses düzey ayarlayıcı: [ncpamixer](https://github.com/fulhax/ncpamixer)
- Ağ yönetim arayüzü: [connman-gtk](https://github.com/etylermoss/connman-gtk-nobluetooth)
- İnternet tarayıcısı: [firefox](mozilla.org)
- Ofis uygulaması: [libreoffice](https://libreoffice.org)
- Dosya yöneticisi: [pcmanfm](https://wiki.lxde.org/en/PCManFM)
- Metin düzenleyici: [geany](https://geany.org)
- Terminal uygulaması: [lxterminal](https://wiki.lxde.org/en/LXTerminal)
- Görev yöneticisi: [lxtask](https://wiki.lxde.org/en/LXTask), [htop](https://github.com/htop-dev/htop)
- Görüntü işleme uygulaması: [gimp](https://www.gimp.org)
- Medya oynatıcısı: [celluloid](https://celluloid-player.github.io)
- Tema ve simge seti: [*milis-gtk-theme](https://github.com/sonakinci41/milis-gtk-theme), [*milis-simge](https://github.com/sonakinci41/Milis-Simge)
- Resim gösterici: [*resimlik](https://mls.akdeniz.edu.tr/git/sonakinci41/Resimlik)
- Ekran parlaklık: [light](https://github.com/haikarainen/light)
- Ekran kitleme: [swaylock](https://github.com/swaywm/swaylock)
- Ekran kayıt edici: [wf-recorder](https://github.com/ammen99/wf-recorder)
- Ekran yakalayıcı: [grim](https://github.com/emersion/grim)
- Ekran alanı seçicisi: [slurp](https://github.com/emersion/slurp)
- Ekran alıntılama: [*meg](https://mls.akdeniz.edu.tr/git//sonakinci41/MEG)
- Arşiv yöneticisi: [xarchiver](https://github.com/ib/xarchiver)
- Pano dinleyici: [clipman](https://github.com/yory8/clipman)
- Pano uygulaması: [wf-clipboard](https://github.com/bugaevc/wl-clipboard)
- Takvim yöneticisi: [gsimplecalc](https://github.com/dmedvinsky/gsimplecal)
- Sistem kurulum arayüzü: [*milis-yukleyici](https://mls.akdeniz.edu.tr/git/milislinux/milis-yukleyici)
- Yetkili çalıştırma arayüzü: [*sudoui](https://mls.akdeniz.edu.tr/git/sonakinci41/RUR)

**Not**: Başında * olanlar Milis Linux'un geliştirdiği özgün uygulamalardır.
 

### Sürüm Bilgileri
- Linux çekirdek 5.14.16
- Linux-firmware 20210818
- GCC 10.2.0 
- LLVM 11.1.0
- Wayland 1.19.0
- GTK3 3.24.28
- QT 5.15.2
- Firefox 96.0.3
- Libreoffice 7.1.4
- Gimp 2.99.6
- Inkscape 1.1
- Postgresql 13.3
- Python 3.9.2
- MPS 2.2

### Tanıtım Videosu
<p>
<iframe src="https://player.vimeo.com/video/685362148?h=7b3e00dbf5" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

### İndirme Bağlantıları

[Masaüstü](https://mls.akdeniz.edu.tr/iso/2.1/milis-2.1-desktop-2022.02.19.iso)

[Minimal (Konsol)](https://mls.akdeniz.edu.tr/iso/2.1/milis-2.1-minimal-2022.02.19.iso)

